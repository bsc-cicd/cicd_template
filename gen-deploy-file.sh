#!/bin/bash
#生成k8s部署文件
# https://gitlab.com/api/v4/projects/35419017/repository/files/gen-deploy-file.sh/raw?ref=main
# curl https://gitlab.com/api/v4/projects/35419017/repository/files/gen-deploy-file.sh/raw?ref=main > gen-deploy-file.sh && chmod +x gen-deploy-file.sh && sh gen-deploy-file.sh
#部署的实例数量
# REPLICAS_COUNT=1
#镜像仓库秘钥
# IMAGE_PULL_SECRETS="k8s-docker-registry"
#容器端口
# CONTAINER_PORT=9000
#k8s部署文件名
# DEPLOY_FILE_NAME="deployment-service.yml"
#项目名
# CI_PROJECT_NAME="minio"
# 容器镜像
# IMAGE_TAG="bitnami/minio"

#shell判断文件,目录是否存在或者具有权限
# -f 参数判断 $file 是否存在
if [ -f "$DEPLOY_FILE_NAME" ]; then

echo '文件已存在';

else

echo '文件不存在';
cat > ${DEPLOY_FILE_NAME} << EOF
apiVersion: apps/v1
kind: Deployment
metadata:
  # Kind 的名称
  name: ${CI_PROJECT_NAME}
spec:
  selector:
    matchLabels:
      # 容器标签的名字，发布 Service 时，selector 需要和这里对应
      app: ${CI_PROJECT_NAME}
  # 部署的实例数量
  replicas: ${REPLICAS_COUNT}
  template:
    metadata:
      labels:
        app: ${CI_PROJECT_NAME}
    spec:
      # 配置容器，数组类型，说明可以配置多个容器
      containers:
      # 容器名称
      - name: ${CI_PROJECT_NAME}
        # 容器镜像
        image: ${IMAGE_TAG}
        # 资源的限制
        resources:
         limits:     
          cpu: 1000m
          memory: 3000Mi
        # 给pod分配的资源
         requests:   
          cpu: 50m
          memory: 100Mi
        # 只有镜像不存在时，才会进行镜像拉取IfNotPresent
        imagePullPolicy: Always
        ports:
        # Pod 端口
        - containerPort: ${CONTAINER_PORT}
      #配置docker镜像私库访问凭证
      imagePullSecrets:
        - name: ${IMAGE_PULL_SECRETS}
---
apiVersion: v1
kind: Service
metadata:
  name: ${CI_PROJECT_NAME}
spec:
  selector:
    app: ${CI_PROJECT_NAME}
  #type: NodePort
  ports:
  - protocol: TCP
    port: ${CONTAINER_PORT}
    targetPort: ${CONTAINER_PORT}
EOF

fi
