#!/bin/bash
#生成java项目dockerfile
# https://gitlab.com/api/v4/projects/35419017/repository/files/gen-java-dockerfile.sh/raw?ref=main
#curl https://gitlab.com/api/v4/projects/35419017/repository/files/gen-java-dockerfile.sh/raw?ref=main > gen-java-dockerfile.sh && chmod +x gen-java-dockerfile.sh && sh gen-java-dockerfile.sh
#DOCKER_FILE_NAME="Dockerfile"
# -f 参数判断 $file 是否存在
if [ -f ../"$DOCKER_FILE_NAME" ]; then

echo '文件已存在';
#复制Dockerfile到当前目录
cp ../$DOCKER_FILE_NAME .

else

echo '文件不存在';
cat > $DOCKER_FILE_NAME << EOF
FROM java:8
ADD app.jar /app.jar
RUN bash -c 'touch /app.jar'
# 设置东八区时区
RUN ln -sf /usr/share/zoneinfo/Asia/Shanghai /etc/localtime
RUN echo 'Asia/Shanghai' >/etc/timezone
EXPOSE 8080
ENTRYPOINT ["java", "-jar","/app.jar"]
EOF

fi
